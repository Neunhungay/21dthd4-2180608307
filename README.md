# 21dthd4-2180608307

# Nguyen Hoang Phuc

# 2180608307

# 21DTHD4

# 21dthd4-2180607692


| Title | Manager update tenant information |
| --- | --- |
| Value Statement | As a Manager, I want to update tenant information |
| Acceptance Criteria | <ins>Acceptance Criteria 1:</ins><br/>Log in to your manager account on the tenant management platform.<br/>Navigate to the "Tenants" or "Residents" section of the platform.<br/>Search for the specific tenant you want to update information for by entering their name or unit number.<br/>Click on the tenant's profile or select the "Edit" option next to their name.<br/><ins><br/>Acceptance Criteria 2:</ins><br/>Update the necessary information such as contact details, emergency contacts, lease details, or any other relevant data.<br/>Double-check the changes for accuracy and completeness.</br>Save the updated information by clicking on the "Save" or "Update" button.<br/>If applicable, notify the tenant about any changes made to their profile and provide them with the updated information.<br/>Remember to handle any sensitive tenant data with confidentiality and ensure compliance with privacy regulations.|
| Owner | Nguyen Hoang Phuc|
| UI | ![Update_information-NuyenHoangPhuc-2180608307](/uploads/1b339577266785a20a52ae908862c112/Update_information-NuyenHoangPhuc-2180608307.jpg) |


|N| Req ID | Test Objected | Test Steps | Expected Result | Actual Result | Pass/Fail | Related Defects |
|-| --- | --- | --- | --- | --- | --- | --- | 
|1| Req-04 | Enter all information to be updated | 1. Update complete and accurate information.. <br/> 2. Click the update button. | All newly updated information is added to the list and displays "Updated successfully!"  | | | |
|2| Req-05 | Enter incorrect information | 1. Enter incorrect information or invalid information. <br/> 2. Click the exit. | Displays the error message "Invalid information" | | | |
|3| Req-06 | Enter missing information | 1. Enter missing information. <br/> 2. Click the update button. | Displays the error message "Please enter full information" | | | |


## phucdat

# Nguyễn Phúc Đạt

# 2180608489

# 21DTHD4
| Title | Manager Create lease contract |
| --- | --- |
| Value Statement | As a manager, I want to create rental contracts |
| Acceptance Criteria | <ins>Acceptance Criteria 1:</ins><br/>when customers want to rent a room <br/> Create a contract clearly stating customer information, rights and obligations of the lessee, date of contract formation and contract expiration, signature <br/> <ins>Acceptance Criteria 2:</ins><br/>when the customer wants to renew the contract <br/> Create a contract with the same information as the old contract and add the contract end date |
| Owner | NguyenPhucDat |

## LeTheHien

# Lê Thế Hiển

# 2180607483

# 21DTHD4

| Title | Manager sign in |
| --- | --- |
| Value Statement | As a Manager, I want to sign in to the program  |
| Acceptance Criteria | <ins>Acceptance Criteria 1:</ins><br/>Given that the manager is enter correct password<br/> When the manager presses the login button </br> Then open UI <br/><ins><br/>Acceptance Criteria 2:</ins><br/>Given that the manager is enter incorrect password <br/>When the manager presses the login button</br> Then make sure the login denied message is displayed|
| Owner | Le The Hien |






 

 ## Nhân

Họ tên: Nguyễn Thành Nhân

MSSV: 2180601029

Lớp: 21DTHD4


| Title | Manager Add tenant information |
| --- | --- |
| Value Statement | As a manager, I want to add tenant information for vertification as well as management. |
| Acceptance Criteria |<ins>Acceptance Criteria 1:</ins><br/> Tenant information is not yet available<br/>When the manager enters all information,<br/>a new one will be added to the list.<br/><br/><ins>Acceptance Criteria 2:</ins><br/> Tenant information is available<br/>When the manager enters complete information,<br/>the list will be updated when there is a change or the screen will be notified of existing information.  |
owner | Nguyễn Thành Nhân |


## Long

##Họ tên: Dương Hoàng Long  
##MSSV:  2180607692
##Lớp: 21DTHD4

| Title | 	Manager make rent bill |
| --- | --- |
| Value Statement | As a Manager, I want to create payment receipts for tenant |
| Acceptance Criteria |<ins>Acceptance Criteria:</ins>When the tenant wants the rent bill,<br>Create and fill in the tenant's name address, invoice date, and total amount payable |
| owner | Dương Hoàng Long |
